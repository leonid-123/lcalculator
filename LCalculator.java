/* 
 * Created by Leonid Ustenko
 * 2015
 * And nothing else matters :)
 * 
 * */

package com.example.lcalculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Main calculator class. Listens for buttons pressing. Makes calculations.
 * Shows the calculation result on display.
 */

public class LCalculator extends Activity {

	private TextView mDisplay;
	private boolean mBeginNewOperation = true;
	private boolean mDisplayIsEmpty = true;
	private boolean mPointEntered;
	private boolean mFunctionPressed;
	private BigDecimal mNum1;
	private BigDecimal mNum2;

	private static ButtonTypes.OperEnum sOperation;
	private static ButtonTypes.FuncEnum sFunction;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);

		mDisplay = (TextView) findViewById(R.id.tvDisplay);
		mDisplay.setText("0");
	}

	/**
	 * listens for numerical buttons pressing and assigns numerical value for
	 * chosen button ID
	 */

	public void num_Clicked(View view) {

		Button button = (Button) view;
		int number = getNumValueByButtonId(button.getId());

		// setting a new digit onto display if it is empty
		if (mDisplayIsEmpty) {

			if ((number == 0)) {
				mDisplay.setText("0");

			} else {
				mDisplay.setText(Integer.toString(number));
				mDisplayIsEmpty = false;
			}

		} else {
			// appending digit if display isn't empty
			mDisplay.append(Integer.toString(number));
		}
	}

	/**
	 * Listens for operational buttons pressing and assigns enum button type
	 */

	public void op_Clicked(View view) {
		Button button = (Button) view;

		/*
		 * calculating and printing result if a number or function has been
		 * pressed before. (not entering here if another operation has been
		 * pressed before)
		 */

		if (mBeginNewOperation) {

			mNum1 = readDisplayState();
			mFunctionPressed = false;
			mBeginNewOperation = false;

		} else {

			// refreshing first operand value if a functional button has been
			// pressed
			if (mFunctionPressed) {
				mNum1 = readDisplayState();
				mFunctionPressed = false;

			} else {
				// getting second operand value
				mNum2 = readDisplayState();

				try {
					printResult(calculateResult(mNum1, sOperation, mNum2));

				} catch (ArithmeticException e) {
					showError();
					return;
				}

				mNum1 = readDisplayState();
			}
		}

		// setting current operation to sOperation variable (by button id).
		setOperation(button.getId());

		// next digit entered will be a new number
		mDisplayIsEmpty = true;
		// typing a point will be possible
		mPointEntered = false;
	}

	/**
	 * listens for functional buttons pressing and assigning enum button type
	 */

	public void func_Clicked(View view) {
		Button button = (Button) view;

		mFunctionPressed = true;

		/*
		 * checking operational buttons id and assigning enum functions value
		 */
		switch (button.getId()) {

		case R.id.buttonAc:
			sFunction = ButtonTypes.FuncEnum.RESET;
			break;

		case R.id.buttonC:
			sFunction = ButtonTypes.FuncEnum.BACKSPACE;
			break;

		case R.id.buttonSign:
			sFunction = ButtonTypes.FuncEnum.SIGN;
			break;

		case R.id.buttonPercent:
			sFunction = ButtonTypes.FuncEnum.PERCENT;
			break;

		case R.id.buttonPoint:
			sFunction = ButtonTypes.FuncEnum.POINT;
			mFunctionPressed = false;
			break;
		}

		// implementing chosen function
		implementFunction(sFunction);
	}

	/** Reads current display number. Returns BigDecimal value */

	private BigDecimal readDisplayState() {
		return new BigDecimal(mDisplay.getText().toString());
	}

	/**
	 * calculates the result of operation between two numbers. Returns
	 * BigDecimal value.
	 */

	private BigDecimal calculateResult(BigDecimal num1,
			ButtonTypes.OperEnum operation, BigDecimal num2) {

		mNum1 = num1;
		mNum2 = num2;
		sOperation = operation;

		BigDecimal result = BigDecimal.ZERO;

		switch (operation) {

		case ADD:
			result = num1.add(num2);
			break;

		case SUBSTRACT:
			result = num1.subtract(num2);
			break;

		case MULTIPLY:
			result = num1.multiply(num2);
			break;

		case EQUAL:
			mDisplayIsEmpty = true;
			break;

		case DIVIDE:
			// rounding the result
			result = num1.divide(num2, 9, RoundingMode.HALF_UP);
			break;
		}
		return result;
	}

	/** Prints the BigDecimal result onto display. 
	 *  If the result's decimal part contains only zero(s)
	 *  Than prints it on display as integer.
	 * */

	private void printResult(BigDecimal result) {

		if (result.doubleValue() % 1 == 0.0) {
			mDisplay.setText(result.setScale(0).toString());

		} else {
			mDisplay.setText(result.toString());
		}
	}

	/**
	 * Getting numerical value by number button id
	 */

	private int getNumValueByButtonId(int numButtonId) {

		int number = 0;

		switch (numButtonId) {

		case R.id.button1:
			number = 1;
			break;

		case R.id.button2:
			number = 2;
			break;

		case R.id.button3:
			number = 3;
			break;

		case R.id.button4:
			number = 4;
			break;

		case R.id.button5:
			number = 5;
			break;

		case R.id.button6:
			number = 6;
			break;

		case R.id.button7:
			number = 7;
			break;

		case R.id.button8:
			number = 8;
			break;

		case R.id.button9:
			number = 9;
			break;

		case R.id.button0:
			number = 0;
			break;
		}

		return number;
	}

	/**
	 * Assigning operation type to static variable sOperation By pressed button
	 * ID.
	 */

	private void setOperation(int buttonId) {

		switch (buttonId) {

		case R.id.buttonPlus:
			sOperation = ButtonTypes.OperEnum.ADD;
			break;

		case R.id.buttonMinus:
			sOperation = ButtonTypes.OperEnum.SUBSTRACT;
			break;

		case R.id.buttonMultiply:
			sOperation = ButtonTypes.OperEnum.MULTIPLY;
			break;

		case R.id.buttonDivide:
			sOperation = ButtonTypes.OperEnum.DIVIDE;
			break;

		case R.id.buttonEqual:
			sOperation = ButtonTypes.OperEnum.EQUAL;
			mBeginNewOperation = true;
			break;
		}
	}

	/**
	 * Implementing function according to pressed button. Current functions read
	 * current display state, changes it and prints the changed result onto
	 * display
	 */

	private void implementFunction(ButtonTypes.FuncEnum function) {
		sFunction = function;

		switch (function) {

		case RESET:
			reset();
			break;

		case BACKSPACE:
			printResult(backSpace());
			break;

		case SIGN:
			printResult(readDisplayState().multiply(new BigDecimal(-1)));
			break;

		case PERCENT:
			printResult(readDisplayState().divide(new BigDecimal(100)));
			break;

		case POINT:
			if (!mPointEntered) {
				if (!mDisplayIsEmpty) {
					mDisplay.append(".");
				} else {
					mDisplay.setText("0.");
					mDisplayIsEmpty = false;
				}
				mPointEntered = true;
			}
			break;
		}
	}

	/**
	 * Deletes the last symbol on display. Prints zero if there is only one
	 * symbol one display
	 * */

	private BigDecimal backSpace() {

		BigDecimal result = BigDecimal.ZERO;
		CharSequence displayState = mDisplay.getText();

		if (displayState.length() > 2) {
			result = new BigDecimal(displayState.subSequence(0,
					displayState.length() - 1).toString());

		} else if (displayState.length() == 2) {

			if ((Double.parseDouble(displayState.toString()) < 0)) {
				mDisplayIsEmpty = true;
				return result;

			} else {
				result = new BigDecimal(displayState.subSequence(0,
						displayState.length() - 1).toString());
			}
		} else if (displayState.length() == 1) {
			mDisplayIsEmpty = true;
			return result;
		}

		return result;
	}

	/**
	 * reseting the calculator in case of pressing AC button
	 */

	private void reset() {
		mDisplay.setText("0");
		mNum1 = mNum2 = BigDecimal.ZERO;
		mDisplayIsEmpty = true;
		mPointEntered = false;
		mFunctionPressed = false;
		mBeginNewOperation = true;
	}

	/**
	 * Prints error message and resets current states
	 */

	private void showError() {
		mDisplay.setText("Error");
		mNum1 = mNum2 = BigDecimal.ZERO;
		mDisplayIsEmpty = true;
		mPointEntered = false;
	}
}
