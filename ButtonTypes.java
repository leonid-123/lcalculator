/* 
 * Created by Leonid Ustenko
 * 2015
 * And nothing else matters :)
 * 
 * */


package com.example.lcalculator;

/**
 * Represents enums for 
 * calculator's operational
 * and functional buttons
 *
 */

public class ButtonTypes {

	static enum OperEnum {
		ADD, SUBSTRACT, MULTIPLY, DIVIDE, EQUAL
	}
	
	static enum FuncEnum {
		RESET, BACKSPACE, SIGN, PERCENT, POINT
	}
}
